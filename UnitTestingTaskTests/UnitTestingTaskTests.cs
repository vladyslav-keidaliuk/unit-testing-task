using UnitTestingTask;

namespace UnitTestingTaskTests
{
    [TestFixture]
    public sealed class Tests
    {
        #region CountingMaxConsecutiveUnequalCharacters

        [TestCase("")]
        [TestCase("        ")]
        [TestCase(null)]
        public void WhenInputStringIsNullOrWhitespace_UnequalCharacters_ThrowsArgumentException(string input)
        {
            // Arrange

            // Act and Assert
            Assert.Throws<ArgumentException>(
                () => SymbolSequenceProcessor.CountingMaxConsecutiveUnequalCharacters(input));
        }

        [TestCase("QWeRTY123", 9)]
        [TestCase("PPPPPPPPP", 1)]
        [TestCase("122345", 4)]
        [TestCase(" rty4444QWERTY ", 7)]
        [TestCase("   Q   W   W  ", 3)]
        [TestCase("A        ", 1)]
        [TestCase("QwER tY", 6)]
        public void WhenInputStringContainsUnequalCharacters_ReturnsExpectedResult(string input, int expectedResult)
        {
            // Arrange

            // Act
            int result = SymbolSequenceProcessor.CountingMaxConsecutiveUnequalCharacters(input);
            // Assert
            Assert.That(result, Is.EqualTo(expectedResult));
        }

        #endregion

        #region CountingMaxConsecutiveLatinLetters

        [TestCase("")]
        [TestCase("        ")]
        [TestCase(null)]
        public void WhenInputStringIsNullOrWhitespace_LatinLetters_ThrowsArgumentException(string input)
        {
            // Arrange

            // Act and Assert
            Assert.Throws<ArgumentException>(
                () => SymbolSequenceProcessor.CountingMaxConsecutiveLatinLetters(input));
        }

        [TestCase("12345A", 1)]
        [TestCase(" 342QWERTY9 ", 1)]
        [TestCase("AAAAABBBBB", 5)]
        [TestCase("342QQQQWERTY9", 4)]
        [TestCase("HelloMMMyWorld", 3)]
        [TestCase("R        ", 1)]
        [TestCase("QW RTTy1D", 2)]
        [TestCase(" ABABABABBA", 2)]
        [TestCase("AaBbCc", 1)]
        [TestCase("#-()+_ ", 0)]
        [TestCase("�����", 0)]
        public void WhenInputStringContainsLatinLetters_ReturnsExpectedResult(string input, int expectedResult)
        {
            // Arrange

            // Act
            int result = SymbolSequenceProcessor.CountingMaxConsecutiveLatinLetters(input);
            // Assert
            Assert.That(result, Is.EqualTo(expectedResult));
        }

        #endregion

        #region CountingMaxConsecutiveDigits

        [TestCase("")]
        [TestCase("        ")]
        [TestCase(null)]
        public void WhenInputStringIsNullOrWhitespace_MaxConsecutiveDigits_ThrowsArgumentException(string input)
        {
            // Arrange

            // Act and Assert
            Assert.Throws<ArgumentException>(() => SymbolSequenceProcessor.CountingMaxConsecutiveDigits(input));
        }

        [TestCase(" 011002220000111 ", 4)]
        [TestCase("Wersdfs12A", 1)]
        [TestCase("Qw(#=+_e-", 0)]
        [TestCase("AA55555ejbujd ", 5)]
        public void WhenInputStringContainsDigits_ReturnsMaxConsecutiveDigitCount(string input, int expectedResult)
        {
            // Arrange

            // Act
            int result = SymbolSequenceProcessor.CountingMaxConsecutiveDigits(input);
            // Assert
            Assert.That(result, Is.EqualTo(expectedResult));
        }

        #endregion
    }
}