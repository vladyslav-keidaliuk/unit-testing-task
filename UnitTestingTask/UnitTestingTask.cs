﻿namespace UnitTestingTask
{
    public static class SymbolSequenceProcessor
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Please input sequence of symbols:");
            String input = Console.ReadLine();

            int maxConsecutiveUnequal = CountingMaxConsecutiveUnequalCharacters(input);
            int maxConsecutiveLetters = CountingMaxConsecutiveLatinLetters(input);
            int maxConsecutiveDigits = CountingMaxConsecutiveDigits(input);

            Console.WriteLine($"For input '{input}':");
            Console.WriteLine($"- The maximum consecutive unequal characters: {maxConsecutiveUnequal}");
            Console.WriteLine($"- The maximum consecutive Latin letters: {maxConsecutiveLetters}");
            Console.WriteLine($"- The maximum consecutive digits: {maxConsecutiveDigits}");
            Console.WriteLine();

        }

        public static int CountingMaxConsecutiveUnequalCharacters(string inputString)
        {

            if (string.IsNullOrWhiteSpace(inputString))
            {
                throw new ArgumentException("Error! String can't be empty");
            }

            inputString = inputString.Trim();

            int max = 0;
            int currentCount = 0;
            char prevChar = inputString[0];


            foreach (var currentChar in inputString)
            {
                if (currentChar != ' ' && currentChar != prevChar)
                {
                    currentCount++;
                }
                else if (currentChar != ' ')
                {
                    currentCount = 1;
                }

                if (currentCount > max)
                {
                    max = currentCount;
                }

                prevChar = currentChar;
            }

            return max;
        }

        public static int CountingMaxConsecutiveLatinLetters(string inputString)
        {
            if (string.IsNullOrWhiteSpace(inputString))
            {
                throw new ArgumentException("Error! String can't be empty");
            }

            int max = 0;
            int currentCount = 0;

            for (var i = 0; i < inputString.Length; i++)
            {
                if (!char.IsLetter(inputString[i]) || !IsLatinLetter(inputString[i])) continue;

                currentCount = (i == 0 || inputString[i] != inputString[i - 1]) ? 1 : currentCount + 1;

                max = Math.Max(max, currentCount);
            }

            return max;
        }



        public static int CountingMaxConsecutiveDigits(string inputString)
        {
            if (string.IsNullOrWhiteSpace(inputString))
            {
                throw new ArgumentException("Error! String can't be empty");
            }

            inputString = inputString.Trim();
            int max = 0;
            int currentCount = 1;


            if (char.IsDigit(inputString[0]) && inputString.Length == 1)
            {
                max = 1;
            }

            int i = 1;
            while (i < inputString.Length)
            {
                if (char.IsDigit(inputString[i]) && char.IsDigit(inputString[i - 1]))
                {
                    currentCount = (inputString[i] == inputString[i - 1]) ? currentCount + 1 : 1;

                    max = Math.Max(max, currentCount);
                }
                i++;
            }

            if (char.IsDigit(inputString[i - 1]))
            {
                max = Math.Max(max, currentCount);
            }

            return max;
        }

        static bool IsLatinLetter(char ch)
        {
            if (char.IsLetter(ch))
            {
                return ch is >= 'A' and <= 'Z' or >= 'a' and <= 'z';
            }
            return false;
        }

    }
}
